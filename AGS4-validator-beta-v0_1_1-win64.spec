# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['gui-ags-checker.py'],
             pathex=['F:\\NextCloud\\GitLab\\validator\\ags-checker-desktop-app\\gui'],
             binaries=[],
             datas=[('dicts\\*.ags', 'dicts')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='AGS4-validator-beta-v0_1)1-win64',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False , icon='assets\\AGS.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='AGS4-validator-beta-v0_1_1-win64')

